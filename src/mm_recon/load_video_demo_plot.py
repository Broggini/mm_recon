# %%
from __future__ import absolute_import, division, print_function

import numpy as np
import rosbag_pandas
import pandas as pd
import matplotlib.pyplot as plt
from tqdm import tqdm
import sys
sys.path.append('/home/denis/catkin_ws/src/mm_recon/src/mm_recon')
import mm_plt

#%%
def bagfile(filename):
    return  "/home/denis/catkin_ws/src/mm_recon/bags/plot_treshold_bags/myo/"+filename+".bag"

bags2 = np.array([[bagfile('myo_05'),0.5],
                 [bagfile('myo_06'),0.6],
                 [bagfile('myo_07'),0.7],
                 [bagfile('myo_075'),0.75],
                 [bagfile('myo_08'),0.8]])

bags25 = np.array([[bagfile('25win/myo_05_smooth'),0.5]])
    
bags3 = np.array([[bagfile('3win/myo_075'),0.75],
                  [bagfile('3win/myo_05'),0.5],
                  [bagfile('3win/myo_085'),0.85],
                  [bagfile('3win/myo_09'),0.9]])


def make_plot(bags,which_window):
    hits = {}
    # Sounds and proba are the same for every bag file
    gestures_proba = rosbag_pandas.bag_to_dataframe(bags[0][0],include=["/pointing_proba"])
    sounds = rosbag_pandas.bag_to_dataframe(bags[0][0],include=["/robotsound"])
    t0_proba = rosbag_pandas.bag_to_dataframe(bags[0][0],include=["/robotsound"]).index[0]
    
    gestures_proba = gestures_proba.set_index((gestures_proba.index - t0_proba).total_seconds())
    sounds = sounds.set_index((sounds.index-t0_proba).total_seconds())
    
    # Plot ax sharex=True,
    fig,(ax,ax2) = plt.subplots(nrows=2,figsize=(15, 6)) 
    ax.set_ylim([0, 1])
    #gestures_proba.plot(kind='line', label='line', ax=ax, color='red')
    ax.plot(gestures_proba,label='model output probability')
    
    for bag in tqdm(bags):
        t0 = rosbag_pandas.bag_to_dataframe(bag[0],include=["/robotsound"]).index[0]
        print(t0)
        gestures = rosbag_pandas.bag_to_dataframe(bag[0],include=["/pointing_debounced"])
        gestures = gestures.set_index((gestures.index-t0).total_seconds())
        hits[bag[1]]=gestures[gestures["pointing_debounced__data"]==1].index
        
    # ..and plot them once
    for treshold,thits in hits.items():
        #==============================================================================
        #         
        #==============================================================================
        tp = []
        fp = []
        sounds_missed = pd.DataFrame(sounds)
        for hit in thits:
            df = sounds[hit-3:hit]
            if len(df) > 0 and len(sounds_missed)>0:
                tp.append(hit)
                sounds_missed = sounds_missed.drop(df.index.values,errors='ignore')
                
            else:
                fp.append(hit)
               
        tp_points = ax2.plot(pd.core.indexes.numeric.Float64Index(tp),
                 [float(treshold)]*len(tp),
                 'go')
        fp_points = ax2.plot(pd.core.indexes.numeric.Float64Index(fp),
                 [float(treshold)]*len(fp),
                 'rx')
        fn_points = ax2.plot(pd.Series(sounds_missed.index[:-1]),
                 [float(treshold)]*(len(sounds_missed)-1),
                 'bo',mfc='none',mec='b')
        ax.axhline(y=treshold,c="blue",linewidth=0.5,alpha=0.5)
        ax2.axhline(y=treshold,c="blue",linewidth=0.5,alpha=0.5)
        
    
    ax.set_title("NN hits using different tresholds - "+which_window,fontdict=mm_plt.title_font)
    ax.set_ylim([0,1])
    ax2.set_ylim([0,1])
    ax.set_xlim([-10,200])
    ax2.set_xlim([-10,200])
    
    ax2.plot([],'go',label='TP')
    ax2.plot([],'rx',label='FP')
    ax2.plot([],'bo',label='FN',mfc='none',mec='b')
    
    idx = pd.Series(sounds.index[:-1])
    ax.vlines(x=idx, ymin=0, ymax=1,linewidth=5, alpha=0.2, color="g",label='sound')
    ax.legend(loc='best', bbox_to_anchor=(1, 0.7))
    ax2.legend(loc='best', bbox_to_anchor=(1, 0.7))
    ax.set_xticks(np.arange(0,210,10))
    ax2.set_xticks(np.arange(0,210,10))
    
    return  fig,ax,ax2,gestures_proba,sounds,hits


fig_plot,plot_proba,plot_hits,gestures_proba,sounds,hits = make_plot(bags25,'2.5 seconds window')