title_font = {'fontsize': 25,
        'fontweight' : 10,
        'verticalalignment': 'baseline',
        'horizontalalignment': 'center'}
axis_font = {'fontsize': 18,
        'fontweight' : 10,
        'verticalalignment': 'center',
        'horizontalalignment': 'center'}

little_title_font = {'fontsize': 18,
        'fontweight' : 10,
        'verticalalignment': 'baseline',
        'horizontalalignment': 'center'}

#%%
#==============================================================================
# 
# import matplotlib.pyplot as plt
# fig,(ax1,ax2) = plt.subplots(nrows=2,figsize=(7, 6),facecolor='green')
# ax1.set_title("Yolo",fontdict=title_font)
# ax1.set_ylabel('Yolo',fontdict=axis_font)
# ax1.set_xlabel('Yolo',fontdict=axis_font)
# ax1.set_facecolor('green')
# facecolor='black'
#==============================================================================
