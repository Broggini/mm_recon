#%%
import numpy as np
import itertools
#%%
#==============================================================================
# Method used by rosnode and pipeline training
#==============================================================================
def mean(window):
    return window.mean().values

def std(window):
    return window.std().values

def minv(window):
    return window.min().values
    
def maxv(window):
    return window.max().values

def delta(window):
    return maxv(window)-minv(window)

def energy(window):
    a = window.values
    return np.einsum('ij,ij->j',a,a)
    

features_used = [mean,std,delta,energy]
sub_windows = 3

def n_columns(features_list=len(features_used),number_of_sensors=7):
    return features_list*sub_windows*number_of_sensors+1

#%%
#==============================================================================
# Code for features comparison
#==============================================================================

def combo_features(x=features_used):
    return itertools.chain(*tuple(itertools.combinations(x, i+1) for i in range(len(x))))

def extract_features(window,features_list=features_used):
    row=[]
    for chunk in np.array_split(window,sub_windows):
        sub_row = np.hstack((f(chunk) for f in features_list))
        row = np.concatenate((row,sub_row),axis=0)
    return row

#==============================================================================
# 
# m = np.array([[f(chunk) for f in feautures_list] for chunk in np.array_split(window,sub_windows)])
# [f(chunk) for f in feautures_list]
#==============================================================================
