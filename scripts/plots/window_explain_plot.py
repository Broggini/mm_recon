# %%
from __future__ import absolute_import, division, print_function

import rosbag_pandas
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import sys
sys.path.append('/home/denis/catkin_ws/src/mm_recon/src/mm_recon')
import mm_plt

def bagfile(filename):
    return  "/home/denis/catkin_ws/src/mm_recon/bags/myo/"+filename+".bag"

def compact_dataframe(bag):
    df1acc = rosbag_pandas.bag_to_dataframe(bag,include=['/myo1/accel'])
    df2acc = rosbag_pandas.bag_to_dataframe(bag,include=['/myo2/accel'])
    
    df = df1acc
    
    df2acc = df2acc.reindex(df1acc.index,method="nearest")
    df = pd.concat([df,df2acc], axis=1)
    
    return df

myo_columns = ['myo1_accel__vector_x',
           'myo1_accel__vector_y',
           'myo1_accel__vector_z',
           'myo2_accel__vector_x',
           'myo2_accel__vector_y',
           'myo2_accel__vector_z']

myodf = compact_dataframe(bagfile('money_in_the_bank'))
snddf =  rosbag_pandas.bag_to_dataframe(bagfile('money_in_the_bank'),include=['/robotsound'])

t0 = snddf.index[0]

myodf = myodf.set_index((myodf.index-t0).total_seconds())
snddf = snddf.set_index((snddf.index-t0).total_seconds())

#%%
toplot = [[7,17,2,4],
          [53,63,12,14],
          #[63,73,14,16],
          [83,93,18,20]]
fig,(ax) = plt.subplots(nrows=len(toplot),figsize=(15, 15))
cnt=0
window_size = 2.5
for plot in toplot:
    # Plot ax
    begin = plot[0]
    end = plot[1]
    lax = ax[cnt]
    cnt+=1
    lax.plot(myodf[begin:end])
    lax.set_xlim([begin,end])
    
    idx = pd.Series(snddf.index[plot[2]:plot[3]])
    
    def validate_TP_window(i,s1=idx[0],s2=idx[1],delta=0.5,x=2):
        # To be a positive window
        # Criteria 1: starts at or before s1 + delta
        if i <= s1+delta:
            # Criteria 2: ends at or before s2 + delta
            if i+window_size <= s2+delta:
                # Criteria 3: ends at or after s1 + x
                if i+window_size >= s1+x:
                    return True
        return False
    
    def validate_TN_window(i,s1=idx[0],s2=idx[1],delta=0.3):
        # To be a non-ambiguous false window
        # Criteria 1: starts at or after s2 - delta
        # Criteria 2: ends at or before s1 + delta (before the pointing movement starts)
        return i > s2-delta or i+window_size < s1+delta
    
    for i in np.arange(begin,end,0.0596):
        w_score = 0
        if validate_TP_window(i):
            w_score+=1
            
        if w_score == 1:
            lax.vlines(x=i, ymin=-2, ymax=2,linewidth=5, alpha=0.5, color="b")
        elif i >= idx[1]-0.5 and i <= idx[1]+0.25:
            lax.vlines(x=i, ymin=-2, ymax=2,linewidth=5, alpha=0.8, color="#FF6666")    
        elif validate_TN_window(i):
            lax.vlines(x=i, ymin=-2, ymax=2,linewidth=5, alpha=0.8, color="#FF6666")
        else:
            lax.vlines(x=i, ymin=-2, ymax=2,linewidth=5, alpha=0.8, color="#C0C0C0")
   
    lax.vlines(x=idx[0], ymin=-2, ymax=2,linewidth=5, alpha=0.8, color="g",label='e0')
    lax.vlines(x=idx[1], ymin=-2, ymax=2,linewidth=5, alpha=0.8, color="r",label='e1')
    
    lax.set_xticks(np.arange(begin,end+0.5,0.5))
    
    lax.set_ylabel('/myo/accel [x9.8 m/s^2]',fontdict=mm_plt.axis_font)
    
    
ax[0].set_title("Discriminating Windows in Myo Data",fontdict=mm_plt.title_font)
#ax[0].set_title("Myo Accelerometer Data",fontdict=mm_plt.title_font)
#==============================================================================
# ax[0].vlines(x=-10, ymin=-2, ymax=2,linewidth=5, alpha=0.8, color="#C0C0C0",label='C0 window starting')
# ax[0].vlines(x=-10, ymin=-2, ymax=2,linewidth=5, alpha=0.5, color="b",label='C1 window starting')
# ax[0].vlines(x=i, ymin=-2, ymax=2,linewidth=5, alpha=0.8, color="#FF6666",label='C2 window starting') 
# ax[0].vlines(x=-10, ymin=-2, ymax=2,linewidth=5, alpha=0.8, color="y",label='Ambiguous window starting')
#==============================================================================
# these are the sounds
ax[0].legend()# bbox_to_anchor=(1, 0.7),loc='best')
ax[len(toplot)-1].set_xlabel('Time (seconds)',fontdict=mm_plt.axis_font)