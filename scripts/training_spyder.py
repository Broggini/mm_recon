# %%
from __future__ import absolute_import, division, print_function

import numpy as np
import rosbag_pandas
import pandas as pd
from sklearn.metrics import accuracy_score
from sklearn import tree
from sklearn import dummy
from sklearn import metrics
from sklearn import ensemble

np.set_printoptions(threshold='nan')
# %%
imudf = rosbag_pandas.bag_to_dataframe("catkin_ws/gesture1.bag", include=["/imu/data"])
snddf = rosbag_pandas.bag_to_dataframe("catkin_ws/gesture1.bag", include=["/robotsound"])

columns=['imu_data__orientation_x', 
         'imu_data__orientation_y',
         'imu_data__orientation_z',
         'imu_data__orientation_w',
         'imu_data__linear_acceleration_x',
         'imu_data__linear_acceleration_y',
         'imu_data__linear_acceleration_z']
window_time=2

# Estrarre window che mi servono e marcarle con 1
# Rimuovere le window dal dataframe

features=[]

for t in snddf.index[:-1]:
    d = imudf[t:(t + pd.Timedelta(seconds=window_time))][columns]
    row = np.hstack((d.mean().values, d.std().values))
    features.append(np.concatenate((row, np.array([1])), axis=0))
    imudf=imudf.drop(d.index.values)

# Produrre window di classe 0
# Pulire il dataset
cnt=0
edge=0
window=[]


print(str("11")+"\t"+str(len(imudf))+" IMUDF"+str(window_time))

while cnt<len(imudf):
    if (cnt+window_time*100)<len(imudf):
        edge=cnt+window_time*100
    else:
        edge=len(imudf)
        
    window=imudf.iloc[cnt:edge].loc[:,columns]
    row = np.hstack((window.mean().values, window.std().values))
    features.append(np.concatenate((row, np.array([0])), axis=0))
    cnt=cnt+window_time*100
    
    
dataset=np.array(features)

# Il dataset è pronto, ora allenare un classificatore
np.random.shuffle(dataset)
yolo=np.vstack(dataset)

training_line = 100

X_tr = yolo[:training_line,:-1]
X_te = yolo[training_line:,:-1]
y_tr = yolo[:training_line,-1:]
y_te = yolo[training_line:,-1:]


clf=tree.DecisionTreeClassifier()
clfPro = ensemble.RandomForestClassifier(n_estimators=25)
clf=clf.fit(X_tr,y_tr)
clfPro.fit(X_tr,y_tr)

accuracy=accuracy_score(y_te,clf.predict(X_te))
accuracyPro=accuracy_score(y_te,clfPro.predict(X_te))
#%%
from sklearn.externals import joblib
joblib.dump(clfPro, 'filename.pkl') 
#%%
#==============================================================================
# scrivi del codice che dati X_tr, X_te, y_tr, y_te stampi:
# numero di sample in ciascuno dei due set;
# prevalenza di ciascuna classe in ciascuno dei due set.
# 
#==============================================================================
def stats(X_tr, X_te, y_tr, y_te):
    
    dum = dummy.DummyClassifier()
    dum.fit(X_tr,y_tr)
    fpr, tpr, _ = metrics.roc_curve(y_te, clf.predict(X_te))
    auc = metrics.auc(fpr, tpr)
    
    
    print ("====================================")
    print ("Sizes===============================")
    print ("X_tr size:\t%d" % len(X_tr))
    print ("X_te size:\t%d" % len(X_te))
    print ("y_tr size:\t%d" % len(y_tr))
    print ("y_tr size:\t%d" % len(y_te))
    print ("====================================")
    print ("Classes=============================")
    print ("y_tr:")
    unique, counts = np.unique(y_tr, return_counts=True)
    print (np.asarray((unique, counts)).T)
    print ("y_te:")
    unique, counts = np.unique(y_te, return_counts=True)
    print (np.asarray((unique, counts)).T)
    print ("====================================")
    print ("Sensor_infos========================")
    for column in columns:
        print (column)
    print ("====================================")
    print ("Accuracy============================")
    print ("Current classifier\t%.5f" % accuracy)
    #print ("Baseline classifier\t%.5f" % counts[0]/(counts[0]+counts[1]))
    print ("Area Under Curve:\t%.5f" % auc)
    
    


























