#!/usr/bin/env python

import rospy, sys
from sound_play.msg import SoundRequest
from sound_play.libsoundplay import SoundClient
import numpy as np
from std_msgs.msg import String


window_time = 2

def sleep(t):
    try:
        rospy.sleep(t)
    except:
        pass


if __name__ == '__main__':
    rospy.init_node('bip')
    each = rospy.get_param('~publish_frequency')
    time = rospy.get_param('~session_time')
    sound_handle = SoundClient()
    sound_handle.stopAll()
    s_point = sound_handle.waveSound('/home/denis/Downloads/up.wav')
    s_stop = sound_handle.waveSound('/home/denis/Downloads/down.wav')

    print "Starting session of %d seconds - publishing each %d seconds" % (time,each)

    cnt = 0

    
    for i in np.arange(15):
        rospy.loginfo("Starting beeps in %d seconds...    \r" % (15-i))
        rospy.sleep(1)
    
    while cnt <= time:
        sys.stdout.write("%d    \r" % (time-cnt))
        sys.stdout.flush()
        if cnt % each == 0:
            rospy.loginfo(rospy.get_rostime())
            s_point.play()
            rospy.sleep(np.random.uniform(window_time,window_time+1))
            s_stop.play()
            cnt += 3
        else:
            rospy.sleep(1)
            cnt += 1

    print 'unplugging'
    sound_handle.play(SoundRequest.NEEDS_UNPLUGGING)
