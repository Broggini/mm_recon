#!/usr/bin/env python

import rospy, sys
from sound_play.msg import SoundRequest

from sound_play.libsoundplay import SoundClient


def sleep(t):
    try:
        rospy.sleep(t)
    except:
        pass


if __name__ == '__main__':
    rospy.init_node('bip')
    each = rospy.get_param('~publish_frequency')
    time = rospy.get_param('~session_time')
    sound_handle = SoundClient()
    sound_handle.stopAll()
    s5 = sound_handle.waveSound("say-beep.wav", 1.0)

    print "Starting session of %d seconds - publishing each %d seconds" % (time,each)

    cnt = 0

    while cnt <= time:
        sys.stdout.write("%d    \r" % (time-cnt))
        sys.stdout.flush()
        if cnt % each == 0:
            s5.play()
        cnt += 1
        rospy.sleep(1)

    print 'unplugging'
    sound_handle.play(SoundRequest.NEEDS_UNPLUGGING)
