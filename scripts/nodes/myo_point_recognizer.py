#!/usr/bin/env python
#%%
from __future__ import absolute_import, division, print_function

import message_filters
from geometry_msgs.msg import Vector3Stamped
from geometry_msgs.msg import QuaternionStamped
from std_msgs.msg import Float32
import collections
import numpy as np
import rospy
import tensorflow as tf
import keras
import thread
import Queue


graph = tf.get_default_graph()

cnt = 0

pubs = []

BUF_SIZE = 1
q = Queue.Queue(BUF_SIZE)

def predict(win_len, pubs):
   th = rospy.get_param('~threshold')
   while True:
        d = q.get()
        if(len(d) != win_len):
            rospy.sleep(0.01)
            continue
        print(".")
        m = np.array([d])
        with graph.as_default():
            prediction = model.predict_proba(m)
            pubs[1].publish(prediction>th)
            pubs[0].publish(prediction)
        rospy.sleep(0.0001)
       

def callback(m1a,m2a,m1r,m2r):
    row = [m1a.vector.x,
           m1a.vector.y,
           m1a.vector.z,
           m1r.quaternion.x,
           m1r.quaternion.y,
           m1r.quaternion.z,
           m1r.quaternion.w,
           m2a.vector.x,
           m2a.vector.y,
           m2a.vector.z,
           m2r.quaternion.x,
           m2r.quaternion.y,
           m2r.quaternion.z,
           m2r.quaternion.w]
    
    d.append(row)
    q.put(d)
  

def listener(custom_topics):
    global pubs
    global model
    global d
    global win_len 
    
    win_len = int((rospy.get_param('~win_length') * 50))
    
    d = collections.deque(maxlen=win_len)
    ts = message_filters.ApproximateTimeSynchronizer([message_filters.Subscriber('myo1/accel',Vector3Stamped),
                                       message_filters.Subscriber('myo2/accel',Vector3Stamped),
                                       message_filters.Subscriber('myo1/rotation',QuaternionStamped),
                                       message_filters.Subscriber('myo2/rotation',QuaternionStamped)],(rospy.get_param('~win_length') * 50), 0.02, allow_headerless=True)
    ts.registerCallback(callback)
    rospy.loginfo('registered callback for message_filters')
    pubs = custom_topics
    model = keras.models.load_model('/home/denis/catkin_ws/src/mm_recon/cls/'+str(rospy.get_param('~cls_name')))
    
    
    thread.start_new_thread (predict, (win_len, pubs) )
    
    rospy.spin()

if __name__ == '__main__':
    
    rospy.init_node('pointing_recognizer', anonymous=True)
    listener([rospy.Publisher('pointing_proba',Float32,queue_size=1),
              rospy.Publisher('pointing',Float32,queue_size=1)])
        
    