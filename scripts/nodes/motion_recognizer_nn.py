#!/usr/bin/env python
#%%
from __future__ import absolute_import, division, print_function

import rospy
from sensor_msgs.msg import Imu
import numpy as np
import collections
from std_msgs.msg import Float32
import tensorflow as tf
 
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning) 

#%%
import keras

d = collections.deque(maxlen=200)
cnt = 0
graph = tf.get_default_graph()

model = keras.models.load_model('/home/denis/catkin_ws/src/mm_recon/cls/pointing.model')
#%%
def callback(data,pubs):
    global cnt
    
    d.append([data.orientation.x,
              data.orientation.y,
              data.orientation.z,
              data.orientation.w,
              data.linear_acceleration.x,
              data.linear_acceleration.y,
              data.linear_acceleration.z])
    cnt+=1
    
    if(len(d)==200 and cnt%10==0):
        m = []
        m.append(d)
        m = np.array(m)
        rospy.loginfo(m.shape)
        with graph.as_default():
            prediction = model.predict_proba(np.array(m))
            pubs[0].publish(prediction>rospy.get_param('~threshold'))
            pubs[1].publish(prediction)
            cnt=0
        
     
         

def listener(g1_pub,g1_proba_pub):
    rospy.init_node('imu_listener', anonymous=True)
    pubs = [g1_pub,g1_proba_pub]
    rospy.Subscriber('/imu/data', Imu, callback,pubs,queue_size=200)
    rospy.spin()

if __name__ == '__main__':
    global threshold
    listener(rospy.Publisher('gesture_1',Float32,queue_size=1),
             rospy.Publisher('gesture_1_proba',Float32,queue_size=1))
