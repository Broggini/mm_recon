#!/usr/bin/env python
from __future__ import absolute_import, division, print_function

import rospy
import tf
import math
from geometry_msgs.msg import QuaternionStamped
import PyKDL as kdl
import numpy as np

def rotated_quaternion(q):
    q = kdl.Rotation.Quaternion(q.quaternion.x,
                                q.quaternion.y,
                                q.quaternion.z,
                                q.quaternion.w)
    
    a = np.array(q.GetQuaternion())
    b = a / np.linalg.norm(a)
    
    original = kdl.Rotation.Quaternion(*b)
    
    rot = kdl.Rotation.Identity()
    rot.DoRotZ(-(math.pi/4))
    
    rotated = rot * original
    
    return rotated.GetQuaternion()

def rotate_quaternion(msg):
    br = tf.TransformBroadcaster()
    br.sendTransform((1, 1, 0),
                     rotated_quaternion(msg),
                     rospy.Time.now(),
                     "myo_rand_rot",
                     "world")

if __name__ == '__main__':
    rospy.init_node('myo_rotator')
    myo = rospy.get_param('~myo')
    rospy.Subscriber('/%s/rotation' % myo,
                     QuaternionStamped,
                     rotate_quaternion)
    rospy.spin()
