#!/usr/bin/env python
import rospy
from std_msgs.msg import Float32
from geometry_msgs.msg import Twist

debouncer = 0
toggle = False

def translate(data):
    global toggle
    
    twist = Twist()
    if data.data==1.0:
        if toggle:
            twist.linear.x = 0.0
        else:
            twist.linear.x = 1.0
        toggle = not(toggle)
        rospy.loginfo("STOP")
        
    return twist
        
def callback(data,pubs):
    global debouncer
    
    
    
    if debouncer <= 0 and data.data > 0.9: #threshold
        rospy.loginfo(rospy.get_caller_id() + "Proba: %.3f, debouncer = %d", data.data,debouncer)
        pubs[0].publish(translate(data))
        pubs[1].publish(1.0)
        debouncer = 30

    debouncer-=1
        
    
        
def listener(pubs):
    rospy.init_node('robot_sentinel', anonymous=True)
    rospy.Subscriber("gesture_1",Float32,callback,pubs)
    rospy.spin()

if __name__ == '__main__':
    pubs = [rospy.Publisher('/cmd_vel', Twist, queue_size=1),
            rospy.Publisher('/gesture_1_debounced', Float32, queue_size=1)]
    listener(pubs)
    