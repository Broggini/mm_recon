#!/usr/bin/env python
#%%
from __future__ import absolute_import, division, print_function

import rospy
from sensor_msgs.msg import Imu
from sklearn.externals import joblib
import numpy as np
import pandas as pd
import collections
from std_msgs.msg import Float32

from mm_recon import features_tool
 
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning) 

#%%
clf = joblib.load('/home/denis/catkin_ws/src/mm_recon/cls/best_classifier_1_1.pkl')
#clf = joblib.load('/home/denis/catkin_ws/src/mm_recon/cls/best_classifier_1_tree.pkl')
#clf = joblib.load('/home/denis/catkin_ws/src/mm_recon/cls/best_classifier_1_SVC.pkl')
#clf = joblib.load('/home/denis/catkin_ws/src/mm_recon/cls/best_classifier_1_GAUSS.pkl')
d = collections.deque(maxlen=200)
cnt = 0
#%%
def callback(data,pubs):
    global cnt
    
    d.append([data.orientation.x,
              data.orientation.y,
              data.orientation.z,
              data.orientation.w,
              data.linear_acceleration.x,
              data.linear_acceleration.y,
              data.linear_acceleration.z])
    cnt+=1
    
    if(len(d)>1 and cnt%10==0):
        n = pd.DataFrame(np.array(d))
        row = features_tool.extract_features(n)
        prediction = clf.predict_proba(row)
        #prediction = clf.predict_proba(row)[0]
        pubs[0].publish(prediction[:,1]>rospy.get_param('~threshold'))
        pubs[1].publish(prediction[:,1])
        cnt=0
        
     
         

def listener(g1_pub,g1_proba_pub):
    rospy.init_node('imu_listener', anonymous=True)
    pubs = [g1_pub,g1_proba_pub]
    rospy.Subscriber('/imu/data', Imu, callback,pubs,queue_size=200)
    rospy.spin()

if __name__ == '__main__':
    global threshold
    listener(rospy.Publisher('gesture_1',Float32,queue_size=1),
             rospy.Publisher('gesture_1_proba',Float32,queue_size=1))
