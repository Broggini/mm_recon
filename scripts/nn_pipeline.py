# %%
from __future__ import absolute_import, division, print_function

import numpy as np
import rosbag_pandas
import pandas as pd

import os.path
from tqdm import tqdm
#==============================================================================
# PARAMETERS + GLOBAL VARS
#==============================================================================

bags_g1 = ["/home/denis/catkin_ws/src/mm_recon/bags/training/gesture1_fp.bag",
         "/home/denis/catkin_ws/src/mm_recon/bags/training/gesture11.bag",
         "/home/denis/catkin_ws/src/mm_recon/bags/training/gesture111.bag",
         "/home/denis/catkin_ws/src/mm_recon/bags/training/gesture11111.bag",
         "/home/denis/catkin_ws/src/mm_recon/bags/training/gesture1111.bag"] 

columns=['imu_data__orientation_x',
         'imu_data__orientation_y',
         'imu_data__orientation_z',
         'imu_data__orientation_w',
         'imu_data__linear_acceleration_x',
         'imu_data__linear_acceleration_y',
         'imu_data__linear_acceleration_z']


clfs_scores = {}

window_seconds = 2

gestures_1 = []
no_gestures = []


def process_bag_file(bagfile):
    
    global gestures_1
    global no_gestures
    
    if os.path.isfile(bagfile) :
        print("Processing %s ..." % bagfile)
    
    imudf = rosbag_pandas.bag_to_dataframe(bagfile, include=["/imu/data"])
    snddf = rosbag_pandas.bag_to_dataframe(bagfile, include=["/robotsound"])
    
    # Label windows near the sound with gesture label

    for sound_event in tqdm(snddf.index[:-1]):# the last sound represents the end-session-notification
        window = pd.DataFrame()
        for t_past in range(500,0,-10): # milliseconds before the soudn has happened. A gesture has duration of avg 1 s
            t = sound_event-pd.Timedelta(milliseconds=t_past)
            first_imu_after_sound = imudf.loc[t:(t + pd.Timedelta(milliseconds=10))]
            if(len(first_imu_after_sound.index)>0):
                window = (imudf.loc[first_imu_after_sound.index[0]:].head(200))[columns]
                gestures_1.append(np.array(window.as_matrix()))
        
        imudf=imudf.drop(window.index.values)
    
    # Label remaining windows with 0
    for start in range(0,200,10):
        cnt=start
        edge=0
        window=[]
        while cnt<len(imudf):
            if (cnt+window_seconds*100)<len(imudf):
                edge=cnt+window_seconds*100
                window=imudf.iloc[cnt:edge].loc[:,columns] 
                no_gestures.append(window.as_matrix())
            cnt=cnt+window_seconds*100
    
for bag in tqdm(bags_g1):
    process_bag_file(bag)
    
gestures_1 = np.array(gestures_1)
no_gestures = np.array(no_gestures)

dataset = []
[dataset.append({'M':w,'y':1}) for w in tqdm(gestures_1)]
[dataset.append({'M':w,'y':0}) for w in tqdm(no_gestures)]
    
df = pd.DataFrame(dataset)

#%%
import matplotlib.pyplot as plt

def pick_samples(df,ns):
    samples=df.sample(n=ns)
    X = np.array(list(samples["M"]))
    y = np.array(list(samples["y"]))
    return X,y

def print_samples(df):
    for m,y in df.values:
        print("Class:"+str(y))
        plt.pcolor((m-np.mean(m,axis=0))/np.std(m,axis=0),cmap="viridis")
        plt.show()
        
#%%
te_set = df.sample(frac=0.3)
tr_set = df.loc[df.index.difference(te_set.index)]



def generator(df,batch_size):
    while True:
        yield(pick_samples(df,batch_size))
#%%
import keras
from keras.models import Sequential
from keras.layers import Activation, Flatten, Dense
from sklearn import metrics

model = Sequential()
model.add(Flatten(input_shape=(200,7)))
model.add(Dense(10))
model.add(Activation('relu'))
model.add(Dense(1))
model.add(Activation('sigmoid'))
model.compile(loss='binary_crossentropy',
              optimizer='rmsprop',
              metrics=['accuracy'])

batch_size = 16
epochs = 50

history = model.fit_generator(
        generator(tr_set,batch_size),
        steps_per_epoch=2000 // batch_size,
        epochs=epochs,
        validation_data=generator(te_set,batch_size),
        validation_steps=1000 // batch_size
        )

#%%
from keras.utils import plot_model

plot_model(model, to_file='model.png')
print("Model structure saved")
keras.models.save_model(model,"model_g1.model")

#%%
from sklearn import metrics
from sklearn.metrics import accuracy_score

X_te = []
y_te = []

for m,y  in tqdm(te_set.values):
    X_te.append(m)
    y_te.append(y)

X_te = np.array(X_te)
y_te = np.array(y_te)

y_score = model.predict_proba(X_te)
y_pred = y_score>0.5
accuracy = accuracy_score(y_te,y_pred)
fpr, tpr, _ = metrics.roc_curve(y_te, y_score)
area_under_curve = metrics.roc_auc_score(y_te, y_score)

#%%

# Plot
y = []
y_hat = []
for i in tqdm(xrange(1000)):
    im = next(generator(df,batch_size))
    y.append(int(im[1][0]))
    y_hat.append(model.predict(im[0])[0,0])
    
fpr, tpr, _ = metrics.roc_curve(y, y_hat)
auc = metrics.auc(fpr, tpr)

plt.figure()
plt.plot([0, 1], [0, 1], 'k--')
plt.plot(fpr, tpr, label='RF')
plt.xlabel('False positive rate')
plt.ylabel('True positive rate')
plt.title('ROC curve')
plt.legend(loc='best')
plt.draw()