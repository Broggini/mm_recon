from distutils.core import setup

setup(
    version='0.0.0',
    scripts=['scripts'],
    packages=['mm_recon'],
    package_dir={'': 'src'}
)